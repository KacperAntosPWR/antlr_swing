tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
  Integer neqStat = 0;
}
prog    : (e+=scp | e+=expr | d+=decl)* -> program(nazwa={$e},deklaracje={$d});

scp     :
        ^(BLOCKSTART {enterBlock();} (e+=scp | e+=expr | d+=decl)* {exitBlock();}) -> block(wyr={$e},deklaracje={$d})
    ;

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> odejmij(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> pomnoz(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> podziel(p1={$e1.st},p2={$e2.st})
        | ^(POW   e1=expr e2=expr) -> poteguj(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) {globals.hasSymbol($i1.text)}? -> podstaw(p1={$ID.text},p2={$e2.st})
        | INT  {numer++;}          -> int(i={$INT.text},j={numer.toString()})
        | ID                       -> id(i={$ID.text})
        | ^(IF warunek=porownaj e2=expr e3=expr?) {numer++;} -> jezeli(e1={$warunek.st},e2={$e2.st},e3={$e3.st},nr={numer.toString()})
    ;
    
porownaj: ^(EQ e1=expr e2=expr)    -> czyRowne(p1={$e1.st},p2={$e2.st})
        | ^(NEQ e1=expr e2=expr) {neqStat++;} -> czyRozne(p1={$e1.st},p2={$e2.st},nr={neqStat.toString()})
    ;