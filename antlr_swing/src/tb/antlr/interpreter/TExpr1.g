tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (e=expr {drukuj ($e.text + " = " + $e.out.toString());} | deklaracja )* ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = dodaj($e1.out, $e2.out);}
        | ^(MINUS e1=expr e2=expr) {$out = odejmij($e1.out, $e2.out);}
        | ^(MUL   e1=expr e2=expr) {$out = pomnoz($e1.out, $e2.out);}
        | ^(DIV   e1=expr e2=expr) {$out = podziel($e1.out, $e2.out);}
        | ^(POW   e1=expr e2=expr) {$out = poteguj($e1.out, $e2.out);}
        | INT                      {$out = getInt($INT.text);}
        | ID                       {$out = pobierzZmienna($ID.text);}
        ;
        catch [Exception e] {drukuj("Wyjątek: ");}
        
deklaracja
        : ^(VAR i1=ID) {stworzZmienna($i1.text);}
        | ^(PODST i1=ID   e2=expr) {ustawZmienna($i1.text, $e2.out); drukuj($i1.text + " = " + $e2.out.toString());}
        ;