package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {
	
	protected GlobalSymbols symbole = new GlobalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer dodaj(Integer e1, Integer e2) {
		return e1 + e2;
	}
	
	protected Integer odejmij(Integer e1, Integer e2) {
		return e1 - e2;
	}
	
	protected Integer pomnoz(Integer e1, Integer e2) {
		return e1 * e2;
	}
	
	protected Integer podziel(Integer e1, Integer e2) throws Exception {
		if(e2 == 0)
			throw new Exception("Nie mozna dzielic przez 0!");
		
		return e1 / e2;
	}
	
	protected Integer poteguj(Integer e1, Integer e2) {
		return (int) Math.pow(e1, e2);
	}
	
	protected void stworzZmienna(String nazwa) {
		symbole.newSymbol(nazwa);
	}
	
	protected Integer ustawZmienna(String nazwa, Integer e1) {
		symbole.setSymbol(nazwa, e1);
		return e1;
	}
	
	protected Integer pobierzZmienna(String nazwa) {
		return symbole.getSymbol(nazwa);
	}
}
