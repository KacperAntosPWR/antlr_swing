grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat | block )+ EOF!;
    
block:
    BLOCKSTART^ (block | stat)* BLOCKEND!
    ;

stat
    : expr NL -> expr
    | VAR ID (PODST expr)? NL -> ^(VAR ID) ^(PODST ID expr)?
//    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | ifElse NL -> ifElse
    | NL ->
    ;

ifElse
    : IF^ ifExpr THEN! (block | stat) (ELSE! (block | stat))?
    ;

ifExpr
    : expr
      ( EQ^ expr 
      | NEQ^ expr
      )?
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : powExpr
      ( MUL^ atom
      | DIV^ atom
      )*
    ;

powExpr
    : atom
    (POW^ powExpr)?
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

VAR :'var';

IF :'if';

ELSE:'else';

THEN:'then';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;
	
BLOCKSTART
  : '{'
  ;

BLOCKEND
  : '}'
  ;
	
// Jak wyżej
EQ
  : '=='
  ;
NEQ
  : '!='
  ;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
    
POW
  : '^'
  ;
